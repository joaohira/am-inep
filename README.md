# Aprendizado de máquina em microdados do Enem e Censo Escolar
Código do trabalho desenvolvido para a disciplina de Aprendizado de Máquina 1, cursada em 2020.1, no Departamento de Computação da Universidade Federal de São Carlos.

Trabalho desenvolvido por João G. V. Hirasawa, Juliana F. Alves, Matheus M. S. Macaia, Paulo H. Dal Bello e Silvia C. Jesus. Entregue na forma de artigo, entitulado "Técnicas de Aprendizado de Máquina para Predição do Desempenho de Escolas no Exame Nacional do Ensino Médio".
